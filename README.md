Versione italiana del gioco LinuxRogue versione 0.3.6

Immagini di anteprima: http://it.tinypic.com/a/b8te79/1

Richiesti per la compilazione: gcc, ncurses, make

I sorgenti originali della versione inglese sono su Rogue Central:
http://coredumpcentral.org/

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Compilazione, pulizia ed esecuzione:

utente@GNULinux:~/Rogue/Sorgenti$ make
utente@GNULinux:~/Rogue/Sorgenti$ rm -rf *.o
utente@GNULinux:~/Rogue/Sorgenti$ mv rogue ..
utente@GNULinux:~/Rogue/Sorgenti$ cd ..
utente@GNULinux:~/Rogue$ ./rogue

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Aiuto in gioco:

? mostra l'elenco dei comandi disponibili
/ chiede al gioco il significato di una lettera

ESC o barra spaziatrice, uscire da un menu o annullare un comando

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

Elenco dei file contenenti stringhe tradotte (si cerchi "T9N"):

machdep.c, object.c, score.old.c, level.c, save.c, throw.c, rogue.h,
ring.c, use.c, inventory.c, monster.c, move.c, message.c, init.c,
special_hit.c, pack.c, hit.c, score.c, instruct.c, play.c, trap.c,
zap.c

-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
