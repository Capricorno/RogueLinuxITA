/*
 *	Instructions command for rogue clone. Alan Cox 1992
 *	Modified Sep 2006 Greg Kennedy - include instructions
 *	 directly in file.
 */

/*
* Italian translation by Capricornus capricornus@openmailbox.org
* Traduzione italiana di Capricornus capricornus@openmailbox.org
* "T9N" stands for / sta per "translation"
* Look for T9N to find the original strings.
* Cercate T9N per trovare le stringhe originali.
* Capricornus - 2016-10-12
*/

#include <ncurses.h>
#include <string.h>
#include <stdio.h>
#include "rogue.h"
#include "instruct.h"
#include "message.h"

/* T9N
"Movement                                                y  k  u",
"        With SHIFT or CTRL to move until you hit         \\ | /",
"        an obstacle                                     h -+- l",
"                                                         / | \\",
"        .        Rest                                   b  j  n",
"        m <direction>    Move onto something without taking it",
"        ,                Pick up item at your location.",
"",
"Command Keys",
"s          search                    i                inventory",
"f          fight                     F                fight (to the death)",
"e          Eat                       q                Quaff",
"r          Read a scroll             d                Drop",
"P          Put on ring               R                Remove a ring",
"CTRL-P     See previous message      >                Descend level",
"<          Ascend level              )                List weapons",
"]          List armour               =                List rings",
"^          Identify trap             I                Short inventory",
"T          Take off                  W                Wear armour",
"w          Wield weapon              c                Call something a name",
"z          Zap something             t<direction>     Throw something",
"v          Version                   Q                Quit",
"CTRL-A     Show average hit points   S                Save game"
*/

char instructions[23][80] = {
"Movimento                                               y  k  u",
"        Con MAIUSC o CTRL per muovervi finché non       \\ | /",
"        colpite un ostacolo                             h -+- l",
"                                                         / | \\",
"        .        Riposo                                 b  j  n",
"        m <direzione>    Muoversi su qualcosa senza prenderla",
"        ,                Raccogliere un oggetto dove siete.",
"",
"Tasti di comando",
"s          cercare                   i                inventario",
"f          combattere                F                comb. (fino alla morte)",
"e          mangiare                  q                tracannare",
"r          leggere una pergamena     d                gettare a terra",
"P          infilarsi un anello       R                sfilarsi un anello",
"CTRL-P     vedere il mess. prec.     >                scendere di un livello",
"<          salire di un livello      )                elencare le armi",
"]          elencare l'armatura       =                elencare gli anelli",
"^          identif. una trappola     I                inventario breve",
"T          togliersi                 W                indossare un'armatura",
"w          impugnare un'arma         c                dare un nome a qualcosa",
"z          fulminare qualcosa        t<direzione>     scagliare qualcosa",
"v          versione                  Q                abbandonare",
"CTRL-A     mostrare i p.v. medi      S                salvare il gioco"};


void Instructions(void)
{
	char buffer[DROWS+1][DCOLS+1];
	int row;
	int i, j;
	
	for (row = 0; row < DROWS; row++)
	{
		for (j = 0; j < DCOLS; j++)
		{
			buffer[row][j] = mvinch(row, j);
		}
		buffer[row][j] = 0;
		clrtoeol();
	}
	move(0, 0);
	for(i = 0; i < DROWS; i++)
	{
		move(i, 0);
		clrtoeol();
	}	
	refresh();
	
	for(i = 0; i < 23; i++)
	{
		move(i, 0);
		clrtoeol();
		mvaddstr(i, 0, instructions[i]);
	}
	refresh();
	
	rgetchar();
	move(0, 0);
	clrtoeol();
	for(i = 0; i < DROWS; i++)
	{
		move(i, 0);
		clrtoeol();
	}
	refresh();
	
	for(i = 0; i < DROWS; i++)
	{
		mvaddstr(i, 0, buffer[i]);
	}
	refresh();
}
