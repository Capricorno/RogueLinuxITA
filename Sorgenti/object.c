/* object.c */

/*
* Italian translation by Capricornus capricornus@openmailbox.org
* Traduzione italiana di Capricornus capricornus@openmailbox.org
* "T9N" stands for / sta per "translation"
* Look for T9N to find the original strings.
* Cercate T9N per trovare le stringhe originali.
* Capricornus - 2016-10-14
*/

#include <ncurses.h>
#include "rogue.h"
#include "hit.h"
#include "inventory.h"
#include "keys.h"
#include "machdep.h"
#include "message.h"
#include "monster.h"
#include "object.h"
#include "pack.h"
#include "random.h"
#include "ring.h"
#include "room.h"
#include "save.h"

object level_objects;
unsigned short dungeon[DROWS][DCOLS];
int foods = 0;
int party_counter;
object *free_list = (object *) 0;
/* T9N - "slime-mold " */
char *fruit = "tortino-di-fango";

fighter rogue = {
        0, 0,           /* armor, weapon */
        0, 0,           /* rings */
        INIT_HP,        /* Hp current */
        INIT_HP,        /* Hp max */
        16, 16,         /* Str */
        {0},            /* pack */
        0,                      /* gold */
        1, 0,           /* exp, exp_points */
        0, 0,           /* row, col */
        '@',            /* char */
        1250            /* moves */
};

struct id id_potions[POTIONS] = {
/* T9N - "blue \0                           ", "of increase strength " */
{100, "blu \0                           ", "di aumento della forza ", 0},
/* T9N - "red \0                            ", "of restore strength " */
{250, "rossa \0                            ", "di recupero della forza ", 0},
/* T9N - "green \0                          ", "of healing " */
{100, "verde \0                          ", "di cura ", 0},
/* T9N - "grey \0                           ", "of extra healing " */
{200, "grigia \0                           ", "di cura extra ", 0},
/* T9N - "brown \0                          ", "of poison " */
{10, "marrone \0                          ", "di veleno ", 0},
/* T9N - "clear \0                          ", "of raise level " */
{300, "chiara \0                          ", "di aumento di livello ", 0},
/* T9N - "pink \0                           ", "of blindness " */
{10, "rosa \0                           ", "di cecità ", 0},
/* T9N - "white \0                          ", "of hallucination " */
{25, "bianca \0                          ", "di allucinazione ", 0},
/* T9N - "purple \0                         ", "of detect monster " */
{100, "purpurea \0                         ", "di rilevamento dei mostri ", 0},
/* T9N - "black \0                          ", "of detect things " */
{100, "nera \0                          ", "di rilevamento delle cose ", 0},
/* T9N - "yellow \0                         ", "of confusion " */
{10, "gialla \0                         ", "di confusione ", 0},
/* T9N - "plaid \0                          ", "of levitation " */
{80, "plaid \0                          ", "di levitazione ", 0},
/* T9N - "burgundy \0                       ", "of haste self " */
{150, "borgogna \0                       ", "di affrettarsi ", 0},
/* T9N - "beige \0                          ", "of see invisible " */
{145, "beige \0                          ", "di visione dell'invisibile ", 0}
};

struct id id_scrolls[SCROLLS] = {
/* T9N - "                                   ", "of protect armor " */
{505, "                                   ", "di protezione dell'armatura ", 0},
/* T9N - "                                   ", "of hold monster " */
{200, "                                   ", "di trattenere il mostro ", 0},
/* T9N - "                                   ", "of enchant weapon " */
{235, "                                   ", "di incantamento dell'arma ", 0},
/* T9N - "                                   ", "of enchant armor " */
{235, "                                   ", "di incantamento dell'armatura ", 0},
/* T9N - "                                   ", "of identify " */
{175, "                                   ", "di identificazione ", 0},
/* T9N - "                                   ", "of teleportation " */
{190, "                                   ", "di teletrasporto ", 0},
/* T9N - "                                   ", "of sleep " */
{25, "                                   ", "di sonno ", 0},
/* T9N - "                                   ", "of scare monster " */
{610, "                                   ", "di spaventare il mostro ", 0},
/* T9N - "                                   ", "of remove curse " */
{210, "                                   ", "di rimozione della maledizione ", 0},
/* T9N - "                                   ", "of create monster " */
{100, "                                   ", "di creazione del mostro ",0},
/* T9N - "                                   ", "of aggravate monster " */
{25, "                                   ", "di esasperare il mostro ",0},
/* T9N - "                                   ", "of magic mapping " */
{180, "                                   ", "di mappatura magica ",0}
};

struct id id_weapons[WEAPONS] = {
        /* T9N - "short bow " */
        {150, "arco corto ", "", 0},
        /* T9N - "darts " */
        {8, "dardi ", "", 0},
        /* T9N - "arrows " */
        {15, "frecce ", "", 0},
        /* T9N - "daggers " */
        {27, "daghe ", "", 0},
        /* T9N - "shurikens " */
        {35, "shuriken ", "", 0},
        /* T9N - "mace " */
        {360, "mazza ", "", 0},
        /* T9N - "long sword " */
        {470, "spada lunga ", "", 0},
        /* T9N - "two-handed sword " */
        {580, "spada a due mani ", "", 0}
};

struct id id_armors[ARMORS] = {
        /* T9N - "leather armor " */
        {300, "armatura di pelle ", "", (UNIDENTIFIED)},
        /* T9N - "ring mail " */
        {300, "armatura ad anelli ", "", (UNIDENTIFIED)},
        /* T9N - "scale mail " */
        {400, "armatura a scaglie ", "", (UNIDENTIFIED)},
        /* T9N - "chain mail " */
        {500, "cotta di maglia ", "", (UNIDENTIFIED)},
        /* T9N - "banded mail " */
        {600, "maglia con bande ", "", (UNIDENTIFIED)},
        /* T9N - "splint mail " */
        {600, "maglia con strisce di ferro ", "", (UNIDENTIFIED)},
        /* T9N - "plate mail " */
        {700, "maglia a piastre ", "", (UNIDENTIFIED)}
};

struct id id_wands[WANDS] = {
        /* T9N - "                                 ", "of teleport away " */
        {25, "                                 ", "di teletrasporto via ",0},
        /* T9N - "                                 ", "of slow monster " */
        {50, "                                 ", "di rallentamento del mostro ", 0},
        /* T9N - "                                 ", "of confuse monster " */
        {45, "                                 ", "di confondere il mostro ",0},
        /* T9N - "                                 ", "of invisibility " */
        {8, "                                 ", "di invisibilità ",0},
        /* T9N - "                                 ", "of polymorph " */
        {55, "                                 ", "di polimorfo ",0},
        /* T9N - "                                 ", "of haste monster " */
        {2, "                                 ", "di affretta il mostro ",0},
        /* T9N - "                                 ", "of sleep " */
        {25, "                                 ", "di sonno ",0},
        /* T9N - "                                 ", "of magic missile " */
        {20, "                                 ", "di dardo magico ",0},
        /* T9N - "                                 ", "of cancellation " */
        {20, "                                 ", "di annullamento ",0},
        /* T9N - "                                 ", "of do nothing " */
        {0, "                                 ", "di non fare niente ",0}
};

struct id id_rings[RINGS] = {
        /* T9N - "                                 ", "of stealth " */
        {250, "                                 ", "di furtività ",0},
        /* T9N - "                                 ", "of teleportation " */
        {100, "                                 ", "di teletrasporto ", 0},
        /* T9N - "                                 ", "of regeneration " */
        {255, "                                 ", "di rigenerazione ",0},
        /* T9N - "                                 ", "of slow digestion " */
        {295, "                                 ", "di digestione lenta ",0},
        /* T9N - "                                 ", "of add strength " */
        {200, "                                 ", "di aggiunta di forza ",0},
        /* T9N - "                                 ", "of sustain strength " */
        {250, "                                 ", "di sostenimento della forza ",0},
        /* T9N - "                                 ", "of dexterity " */
        {250, "                                 ", "di destrezza ",0},
        /* T9N - "                                 ", "of adornment " */
        {25, "                                 ", "di ornamento ",0},
        /* T9N - "                                 ", "of see invisible " */
        {300, "                                 ", "di vista dell'invisibile ",0},
        /* T9N - "                                 ", "of maintain armor " */
        {290, "                                 ", "di mantenimento dell'armatura ",0},
        /* T9N - "                                 ", "of searching " */
        {270, "                                 ", "di ricerca ",0},
};

extern int cur_level;
extern short max_level;
extern short party_room;
extern char *error_file;
extern boolean is_wood[];
extern room rooms[];
extern object level_monsters;

unsigned short gr_what_is(void);
void make_party(void);
int next_party(void);
void rand_place(object *);
void put_gold(void);
void plant_gold(int, int, boolean);
void gr_scroll(object *);
void gr_potion(object *);
void gr_weapon(object *, int);
void gr_armor(object *);
void gr_wand(object *);

void put_objects(void)
{
        int i, n;
        object *obj;

        if (cur_level < max_level)
        {
                return;
        }
        n = coin_toss() ? get_rand(2, 4) : get_rand(3, 5);
        while (rand_percent(33))
        {
                n++;
        }
        if (cur_level == party_counter)
        {
                make_party();
                party_counter = next_party();
        }
        for (i = 0; i < n; i++)
        {
                obj = gr_object();
                rand_place(obj);
        }
        put_gold();
}

void put_gold(void)
{
        int i, j;
        int row,col;
        boolean is_maze, is_room;

        for (i = 0; i < MAXROOMS; i++)
        {
                is_maze = (rooms[i].is_room & R_MAZE) ? 1 : 0;
                is_room = (rooms[i].is_room & R_ROOM) ? 1 : 0;

                if (!(is_room || is_maze))
                {
                        continue;
                }
                if (is_maze || rand_percent(GOLD_PERCENT))
                {
                        for (j = 0; j < 50; j++)
                        {
                                row = get_rand(rooms[i].top_row + 1, rooms[i].bottom_row - 1);
                                col = get_rand(rooms[i].left_col + 1, rooms[i].right_col - 1);
                                if ((dungeon[row][col] == FLOOR) ||     (dungeon[row][col] == TUNNEL))
                                {
                                        plant_gold(row, col, is_maze);
                                        break;
                                }
                        }
                }
        }
}

void plant_gold(int row, int col, boolean is_maze)
{
        object *obj;

        obj = alloc_object();
        obj->row = row;
        obj->col = col;
        obj->what_is = GOLD;
        obj->quantity = get_rand((2 * cur_level), (16 * cur_level));
        if (is_maze)
        {
                obj->quantity += obj->quantity / 2;
        }
        dungeon[row][col] |= OBJECT;
        (void) add_to_pack(obj, &level_objects, 0);
}

void place_at(object *obj, int row, int col)
{
        obj->row = row;
        obj->col = col;
        dungeon[row][col] |= OBJECT;
        (void) add_to_pack(obj, &level_objects, 0);
}

object * object_at(object *pack, short row, short col)
{
        object *obj;

        obj = pack->next_object;

        while (obj && ((obj->row != row) || (obj->col != col)))
        {
                obj = obj->next_object;
        }
        return(obj);
}

object * get_letter_object(short ch)
{
        object *obj;

        obj = rogue.pack.next_object;

        while (obj && (obj->ichar != ch))
        {
                obj = obj->next_object;
        }
        return(obj);
}

void free_stuff(object *objlist)
{
        object *obj;

        while (objlist->next_object)
        {
                obj = objlist->next_object;
                objlist->next_object = objlist->next_object->next_object;
                free_object(obj);
        }
}

char * name_of(object *obj)
{
        char *retstring;

        switch(obj->what_is)
        {
        case SCROLL:
                /* T9N - "scrolls " : "scroll " */
                retstring = obj->quantity > 1 ? "pergamene " : "pergamena ";
                break;
        case POTION:
                /* T9N - "potions " : "potion " */
                retstring = obj->quantity > 1 ? "pozioni " : "pozione ";
                break;
        case FOOD:
                if (obj->which_kind == RATION)
                {
                        /* T9N - "food " */
                        retstring = "cibo ";
                }
                else
                {
                        retstring = fruit;
                }
                break;
        case WAND:
                /* T9N - "staff " : "wand " */
                retstring = is_wood[obj->which_kind] ? "bastone " : "bacchetta ";
                break;
        case WEAPON:
                switch(obj->which_kind)
                {
                case DART:
                        /* T9N - "darts " : "dart " */
                        retstring=obj->quantity > 1 ? "dardi " : "dardo ";
                        break;
                case ARROW:
                        /* T9N - "arrows " : "arrow " */
                        retstring=obj->quantity > 1 ? "frecce " : "freccia ";
                        break;
                case DAGGER:
                        /* T9N - "daggers " : "dagger " */
                        retstring=obj->quantity > 1 ? "pugnali " : "pugnale ";
                        break;
                case SHURIKEN:
                        /* T9N - "shurikens ":"shuriken " */
                        retstring=obj->quantity > 1?"shuriken ":"shuriken ";
                        break;
                default:
                        retstring = id_weapons[obj->which_kind].title;
                }
                break;
        case ARMOR:
                /* T9N - "armor " */
                retstring = "armatura ";
                break;
        case RING:
                /* T9N - "ring " */
                        retstring = "anello ";
                break;
        case AMULET:
                /* T9N - "amulet " */
                retstring = "amuleto ";
                break;
        default:
                /* T9N - "unknown " */
                retstring = "sconosciut* ";
                break;
        }
        return(retstring);
}

object * gr_object(void)
{
        object *obj;

        obj = alloc_object();

        if (foods < (cur_level / 2))
        {
                obj->what_is = FOOD;
                foods++;
        }
        else
        {
                obj->what_is = gr_what_is();
        }
        switch(obj->what_is)
        {
        case SCROLL:
                gr_scroll(obj);
                break;
        case POTION:
                gr_potion(obj);
                break;
        case WEAPON:
                gr_weapon(obj, 1);
                break;
        case ARMOR:
                gr_armor(obj);
                break;
        case WAND:
                gr_wand(obj);
                break;
        case FOOD:
                get_food(obj, 0);
                break;
        case RING:
                gr_ring(obj, 1);
                break;
        }
        return(obj);
}

unsigned short gr_what_is(void)
{
        int percent;
        unsigned short what_is;

        percent = get_rand(1, 91);

        if (percent <= 30)
        {
                what_is = SCROLL;
        } else if (percent <= 60)
        {
                what_is = POTION;
        } else if (percent <= 64)
        {
                what_is = WAND;
        } else if (percent <= 74)
        {
                what_is = WEAPON;
        } else if (percent <= 83)
        {
                what_is = ARMOR;
        } else if (percent <= 88)
        {
                what_is = FOOD;
        } else
        {
                what_is = RING;
        }
        return(what_is);
}

void gr_scroll(object *obj)
{
        int percent;

        percent = get_rand(0, 85);

        obj->what_is = SCROLL;

        if (percent <= 5)
        {
                obj->which_kind = PROTECT_ARMOR;
        } else if (percent <= 11)
        {
                obj->which_kind = HOLD_MONSTER;
        } else if (percent <= 20)
        {
                obj->which_kind = CREATE_MONSTER;
        } else if (percent <= 35)
        {
                obj->which_kind = IDENTIFY;
        } else if (percent <= 43)
        {
                obj->which_kind = TELEPORT;
        } else if (percent <= 50)
        {
                obj->which_kind = SLEEP;
        } else if (percent <= 55)
        {
                obj->which_kind = SCARE_MONSTER;
        } else if (percent <= 64)
        {
                obj->which_kind = REMOVE_CURSE;
        } else if (percent <= 69)
        {
                obj->which_kind = ENCH_ARMOR;
        } else if (percent <= 74)
        {
                obj->which_kind = ENCH_WEAPON;
        } else if (percent <= 80)
        {
                obj->which_kind = AGGRAVATE_MONSTER;
        } else
        {
                obj->which_kind = MAGIC_MAPPING;
        }
}

void gr_potion(object *obj)
{
        int percent;

        percent = get_rand(1, 118);

        obj->what_is = POTION;

        if (percent <= 5)
        {
                obj->which_kind = RAISE_LEVEL;
        } else if (percent <= 15)
        {
                obj->which_kind = DETECT_OBJECTS;
        } else if (percent <= 25)
        {
                obj->which_kind = DETECT_MONSTER;
        } else if (percent <= 35)
        {
                obj->which_kind = INCREASE_STRENGTH;
        } else if (percent <= 45)
        {
                obj->which_kind = RESTORE_STRENGTH;
        } else if (percent <= 55)
        {
                obj->which_kind = HEALING;
        } else if (percent <= 65)
        {
                obj->which_kind = EXTRA_HEALING;
        } else if (percent <= 75)
        {
                obj->which_kind = BLINDNESS;
        } else if (percent <= 85)
        {
                obj->which_kind = HALLUCINATION;
        } else if (percent <= 95)
        {
                obj->which_kind = CONFUSION;
        } else if (percent <= 105)
        {
                obj->which_kind = POISON;
        } else if (percent <= 110)
        {
                obj->which_kind = LEVITATION;
        } else if (percent <= 114)
        {
                obj->which_kind = HASTE_SELF;
        } else
        {
                obj->which_kind = SEE_INVISIBLE;
        }
}

extern void set_weapon_damage(object *obj)
{
        switch(obj->which_kind)
        {
        case BOW:
        case DART:
                obj->damage = "1d1";
                break;
        case ARROW:
                obj->damage = "1d2";
                break;
        case DAGGER:
                obj->damage = "1d3";
                break;
        case SHURIKEN:
                obj->damage = "1d4";
                break;
        case MACE:
                obj->damage = "2d3";
                break;
        case LONG_SWORD:
                obj->damage = "3d4";
                break;
        case TWO_HANDED_SWORD:
                obj->damage = "4d5";
                break;
        }
}

void gr_weapon(object *obj, int assign_wk)
{
        short percent;
        short i;
        short blessing, increment;

        obj->what_is = WEAPON;
        if (assign_wk)
        {
                obj->which_kind = get_rand(0, (WEAPONS - 1));
        }
        if ((obj->which_kind == ARROW) || (obj->which_kind == DAGGER)
            || (obj->which_kind == SHURIKEN) | (obj->which_kind == DART))
        {
                obj->quantity = get_rand(3, 15);
                obj->quiver = get_rand(0, 126);
        }
        else
        {
                obj->quantity = 1;
        }
        obj->hit_enchant = obj->d_enchant = 0;

        percent = get_rand(1, 96);
        blessing = get_rand(1, 3);

        if (percent <= 16)
        {
                increment = 1;
        }
        else
        {
                if (percent <= 32)
                {
                        increment = -1;
                        obj->is_cursed = 1;
                }
        }
        if (percent <= 32)
        {
                for (i = 0; i < blessing; i++)
                {
                        if (coin_toss())
                        {
                                obj->hit_enchant += increment;
                        }
                        else
                        {
                                obj->d_enchant += increment;
                        }
                }
        }
        set_weapon_damage(obj);
}

void gr_armor(object *obj)
{
        int percent;
        int blessing;

        obj->what_is = ARMOR;
        obj->which_kind = get_rand(0, (ARMORS - 1));
        obj->oclass = obj->which_kind + 2;
        if ((obj->which_kind == PLATE) || (obj->which_kind == SPLINT))
        {
                obj->oclass--;
        }
        obj->is_protected = 0;
        obj->d_enchant = 0;

        percent = get_rand(1, 100);
        blessing = get_rand(1, 3);

        if (percent <= 16)
        {
                obj->is_cursed = 1;
                obj->d_enchant -= blessing;
        }
        else
        {
                if (percent <= 33)
                {
                        obj->d_enchant += blessing;
                }
        }
}

void gr_wand(object *obj)
{
        obj->what_is = WAND;
        obj->which_kind = get_rand(0, (WANDS - 1));
        if (obj->which_kind == MAGIC_MISSILE)
        {
                obj->oclass = get_rand(6, 12);
        } else if (obj->which_kind == CANCELLATION)
        {
                obj->oclass = get_rand(5, 9);
        } else
        {
                obj->oclass = get_rand(3, 6);
        }
}

void get_food(object *obj, boolean force_ration)
{
        obj->what_is = FOOD;

        if (force_ration || rand_percent(80))
        {
                obj->which_kind = RATION;
        } else
        {
                obj->which_kind = FRUIT;
        }
}

void put_stairs(void)
{
        int row, col;

        gr_row_col(&row, &col, (FLOOR | TUNNEL));
        dungeon[row][col] |= STAIRS;
}

int get_armor_class(object *obj)
{
        if (obj)
        {
                return(obj->oclass + obj->d_enchant);
        }
        return(0);
}

object * alloc_object(void)
{
        object *obj;

        if (free_list)
        {
                obj = free_list;
                free_list = free_list->next_object;
        }
        else
        {
                if (!(obj = (object *) md_malloc(sizeof(object))))
                {
                        /* T9N - "Cannot allocate object, saving game." */
                        message("Impossibile allocare l'oggetto, salvataggio del gioco.", 0);
                        save_into_file(error_file);
                }
        }
        obj->quantity = 1;
        obj->ichar = 'L';
        obj->picked_up = obj->is_cursed = 0;
        obj->in_use_flags = NOT_USED;
        obj->identified = UNIDENTIFIED;
        obj->damage = "1d1";
        return(obj);
}

void free_object(object *obj)
{
        obj->next_object = free_list;
        free_list = obj;
}

void make_party(void)
{
        int n;

        party_room = gr_room();

        n = rand_percent(99) ? party_objects(party_room) : 11;
        if (rand_percent(99))
        {
                party_monsters(party_room, n);
        }
}

void show_objects(void)
{
        object *obj;
        short mc, rc, row, col;
        object *monster;

        obj = level_objects.next_object;

        while (obj)
        {
                row = obj->row;
                col = obj->col;

                rc = get_mask_char(obj->what_is);

                if (dungeon[row][col] & MONSTER)
                {
                        if ((monster = object_at(&level_monsters, row, col)))
                        {
                                monster->trail_char = rc;
                        }
                }
                mc = mvinch(row, col);
                if (((mc < 'A') || (mc > 'Z')) &&
                        ((row != rogue.row) || (col != rogue.col)))
                {
                        mvaddch(row, col, rc);
                }
                obj = obj->next_object;
        }

        monster = level_monsters.next_object;

        while (monster)
        {
                if (monster->m_flags & IMITATES)
                {
                        mvaddch(monster->row, monster->col, (int) monster->disguise);
                }
                monster = monster->next_monster;
        }
}

void put_amulet(void)
{
        object *obj;

        obj = alloc_object();
        obj->what_is = AMULET;
        rand_place(obj);
}

void rand_place(object *obj)
{
        int row, col;

        gr_row_col(&row, &col, (FLOOR | TUNNEL));
        place_at(obj, row, col);
}

void new_object_for_wizard(void)
{
        short ch, max, wk;
        object *obj;
        char buf[80];

        if (pack_count((object *) 0) >= MAX_PACK_COUNT)
        {
                /* T9N - "Pack full." */
                message("Il pacco è pieno.", 0);
                return;
        }
        /* T9N - "Type of object?" */
        message("Tipo di oggetto?", 0);

        while (r_index("!?:)]=/,\033", (ch = rgetchar()), 0) == -1)
        {
                sound_bell();
        }
        check_message();

        if (ch == ROGUE_KEY_CANCEL)
        {
                return;
        }
        obj = alloc_object();

        switch(ch)
        {
        case '!':
                obj->what_is = POTION;
                max = POTIONS - 1;
                break;
        case '?':
                obj->what_is = SCROLL;
                max = SCROLLS - 1;
                break;
        case ',':
                obj->what_is = AMULET;
                /* TODO: Need to set max to proper value.
                 * Setting to zero for now.
                 */
                max = 0;
                break;
        case ':':
                get_food(obj, 0);
                /* TODO: Need to set max to proper value.
                 * Setting to zero for now.
                 */
                max = 0;
                break;
        case ')':
                gr_weapon(obj, 0);
                max = WEAPONS - 1;
                break;
        case ']':
                gr_armor(obj);
                max = ARMORS - 1;
                break;
        case '/':
                gr_wand(obj);
                max = WANDS - 1;
                break;
        case '=':
                max = RINGS - 1;
                obj->what_is = RING;
                break;
        default:
                /* TODO: Need to handle the default case.
                 * Filling with foo value now.
                 */
                max = 0;
                break;
        }
        if ((ch != ',') && (ch != ':'))
        {
GIL:
                /* T9N - "Which kind?" */
                if (get_input_line("Che genere?", buf, "", "", 0, 1))
                {
                        wk = get_number(buf);
                        if ((wk >= 0) && (wk <= max))
                        {
                                obj->which_kind = (unsigned short) wk;
                                if (obj->what_is == RING)
                                {
                                        gr_ring(obj, 0);
                                }
                        }
                        else
                        {
                                sound_bell();
                                goto GIL;
                        }
                }
                else
                {
                        free_object(obj);
                        return;
                }
        }
        get_desc(obj, buf);
        message(buf, 0);
        (void) add_to_pack(obj, &rogue.pack, 1);
}

int next_party(void)
{
        int n;

        n = cur_level;
        while (n % PARTY_TIME)
        {
                n++;
        }
        return(get_rand((n + 1), (n + PARTY_TIME)));
}
